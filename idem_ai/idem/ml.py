def __virtual__(hub):  # noqa: N807
    # Ensure that pop_ml's config gets loaded when idem is loaded
    hub.idem.CONFIG_LOAD.append("pop_ml")
    return True


def __init__(hub):  # noqa: N807
    # Add pop-ml's namespace to the hub
    hub.pop.sub.add(dyne_name="ml")
