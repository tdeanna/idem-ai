def pre_display(hub, ctx):
    # Translate docstrings
    if hub.SUBPARSER == "doc":
        if "idem" not in hub.OPT:
            return

        # The user opted out of translating docs
        if not hub.OPT.idem.translate_docs:
            return

        # Missing configuration, cannot use this for display
        if not (hub.OPT.pop_ml.dest_lang or hub.OPT.pop_ml.model_name):
            hub.log.warning(
                "Either a dest_lang or model_name needs to be set in the config file. "
                "I.E. \npop_ml:\n  dest_lang: es"
            )
            return

        # Call the idempotent "init" of pop-ml
        hub.ml.tokenizer.init(
            model_name=hub.OPT.pop_ml.model_name,
            dest_lang=hub.OPT.pop_ml.dest_lang,
            source_lang=hub.OPT.pop_ml.source_lang,
            pretrained_model=hub.OPT.pop_ml.pretrained_model_class,
            pretrained_tokenizer=hub.OPT.pop_ml.pretrained_tokenizer_class,
        )

        # Iteratively search for doc strings in a nested dictionary
        def _translate_docs(d: dict):
            for key, value in d.items():
                if key == "doc":
                    # Translate the value of the doc
                    translation = hub.ml.tokenizer.translate([d["doc"]])
                    # Modify it in place
                    d[key] = translation[0]
                elif isinstance(value, dict):
                    _translate_docs(value)

        # Translate the docs in place
        if isinstance(ctx.args[1], dict):
            _translate_docs(ctx.args[1])


def post_display(hub, ctx):
    """Translate the data using pop-ml."""
    # The user opted out of translating output
    if not hub.OPT.idem.translate_output:
        return ctx.ret

    # Missing configuration, cannot use this for display
    if not (hub.OPT.pop_ml.dest_lang or hub.OPT.pop_ml.model_name):
        hub.log.warning(
            "Either a dest_lang or model_name needs to be set in the config file. "
            "I.E. \npop_ml:\n  dest_lang: es"
        )
        return ctx.ret

    # Call the idempotent "init" of pop-ml
    hub.ml.tokenizer.init(
        model_name=hub.OPT.pop_ml.model_name,
        dest_lang=hub.OPT.pop_ml.dest_lang,
        source_lang=hub.OPT.pop_ml.source_lang,
        pretrained_model=hub.OPT.pop_ml.pretrained_model_class,
        pretrained_tokenizer=hub.OPT.pop_ml.pretrained_tokenizer_class,
    )

    # Translate the output data
    ret = hub.ml.tokenizer.translate([ctx.ret])
    return ret[0]
